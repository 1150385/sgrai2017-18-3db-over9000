#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)
#define MAX(x, y)       (x > y ? x : y)
#define MIN(x, y)       (x < y ? x : y)

#define DEBUG                 0

#define WIDTH_FIELD           400
#define HEIGHT_FIELD          200

#define WIDTH_RACKET          10
#define HEIGHT_RACKET         50
#define VELOCITY_RACKET       250
#define ACCEL_RACKET          3500

#define SERVICE_LINE          50

#define BALL_VELOCITY         180
#define MAX_BALL_VELOCITY     350
#define BALL_DIRECTION_CHANGE 50
#define FADEAWAY_SPIN         0.5
#define INFLUENCE_SPIN        3
#define ANGLE_LIMIT           80
#define BALL_SIZE             10

#define FPS                   60
#define TICKS_PER_SECOND      300

/* VARIAVEIS GLOBAIS */

class Keys {
public:
  GLboolean   q,a,p,l;
};

class State {
public:
  GLboolean   doubleBuffer;
  Keys        keys;
  GLuint      menu_vel_ball_id;
  GLuint      menu_tam_ball_id;
  GLuint      menu_vel_raq_id;
  GLuint      menu_tam_raq_id;
  GLuint      menu_id;
  GLboolean   activeMenu;
  GLboolean   debug;
};

class Racket {
public:
  GLfloat       x,y;
  GLfloat       ySpeed;
  GLint         score;
};

class Ball {
public:
  GLfloat       x,y;
  GLint         speed;
  GLfloat       direction;
  GLfloat       spin;
  GLint			    tamanho;
};

class Model {
public:
  Racket      players[2];
  Ball        ball;
  GLint       alturaRackets;
  GLint       speedRackets;
  GLint       service;
  GLboolean   paused;
};

State state;
Model model;

double reverse_angle (double angle) {
  angle = M_PI - angle;
  while (angle > M_PI) angle -= 2*M_PI;
  while (angle < -M_PI) angle += 2*M_PI;
  return angle;
}

void start_game() {
  model.service= !model.service;

  model.players[0].ySpeed = 0.0f;
  model.players[1].ySpeed = 0.0f;
  model.players[0].x = SERVICE_LINE;
  model.players[1].x = WIDTH_FIELD-SERVICE_LINE;
  model.players[0].y = model.players[1].y = HEIGHT_FIELD*.5;
  model.ball.x=WIDTH_FIELD*.5;
  model.ball.y=HEIGHT_FIELD*.5;
  model.ball.spin=0.0;
  model.ball.speed = BALL_VELOCITY;

  model.ball.direction = RAD(rand()%(2*ANGLE_LIMIT)-ANGLE_LIMIT);
  if(model.service)
    model.ball.direction = reverse_angle(model.ball.direction);
}

/* Inicialização do ambiente OPENGL */
void init(void) {
  srand((unsigned)time(NULL));

  model.ball.tamanho = BALL_SIZE;
  model.alturaRackets = HEIGHT_RACKET;
  model.speedRackets = VELOCITY_RACKET;
  model.service = 2;
  model.players[0].score = model.players[1].score = 0;
  model.paused = GL_FALSE;

  state.activeMenu = GL_FALSE;

  state.keys.a=state.keys.q=state.keys.l=state.keys.p=GL_FALSE;

  start_game();

  glClearColor(0.0, 0.0, 0.0, 0.0);

  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);

  glutIgnoreKeyRepeat(GL_TRUE);
}

/**************************************
 ***  callbacks de janela/desenho   ***
 **************************************/

// CALLBACK PARA REDIMENSIONAR JANELA
void Reshape(int width, int height) {
  // glViewport(botom, left, width, height)
  // define parte da janela a ser utilizada pelo OpenGL
  glViewport(0, 0, (GLint) width, (GLint) height);

  // Matriz Projeccao
  // Matriz onde se define como o mundo e apresentado na janela
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  // gluOrtho2D(left,right,bottom,top); 
  // projeccao ortogonal 2D, com profundidade (Z) entre -1 e 1
  gluOrtho2D(0, WIDTH_FIELD, 0, HEIGHT_FIELD);

  // Matriz Modelview
  // Matriz onde são realizadas as tranformacoes dos models desenhados
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void strokeString(char *str,double x, double y, double z, double s) {
  glPushMatrix();
  glColor3d(0.0, 0.0, 0.0);
  glTranslated(x,y,z);
  glScaled(s,s,s);

  for (;*str;str++)
    glutStrokeCharacter(GLUT_STROKE_ROMAN,*str);

  glPopMatrix();
}

void bitmapString(char *str, double x, double y) {
  // fonte pode ser:
  // GLUT_BITMAP_8_BY_13
  // GLUT_BITMAP_9_BY_15
  // GLUT_BITMAP_TIMES_ROMAN_10
  // GLUT_BITMAP_TIMES_ROMAN_24
  // GLUT_BITMAP_HELVETICA_10
  // GLUT_BITMAP_HELVETICA_12
  // GLUT_BITMAP_HELVETICA_18
  //
  // int glutBitmapWidth  	(	void *font , int character);
  // devolve a largura de um carácter
  //
  // int glutBitmapLength 	(	void *font , const unsigned char *string );
  // devolve a largura de uma string (soma da largura de todos os caracteres)
  glRasterPos2d(x,y);
  for (;*str;str++)
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,(int)*str);
}

void bitmapCenterString(const char *str, double x, double y) {
  glRasterPos2d(x-glutBitmapLength(GLUT_BITMAP_HELVETICA_18,
        (const unsigned char *)str)*0.5,y);
  for (;*str;str++)
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,(int)*str);
}

void draw_field(int p1, int p2) {
  size_t str_length = 255;
  char str[str_length];

  glLineWidth(3);
  glLineStipple(2,0xFF00);

  glEnable(GL_LINE_STIPPLE);
  glBegin(GL_LINES);
  glColor3f(1.0f ,1.0f ,1.0f);
  glVertex2f(WIDTH_FIELD*.5,0);
  glVertex2f(WIDTH_FIELD*.5,HEIGHT_FIELD);
  glEnd();
  glDisable(GL_LINE_STIPPLE);

  snprintf(str,str_length,"%d",p1);
  bitmapString(str, WIDTH_FIELD*.4,HEIGHT_FIELD*.9);
  snprintf(str,str_length,"%d",p2);
  bitmapString(str, WIDTH_FIELD*.6,HEIGHT_FIELD*.9);

  if(state.debug) {
    snprintf(str,255,"ang:%.1f*PI\n x:%.1f\n y:%.1f ",
        model.ball.direction/M_PI,model.ball.x,model.ball.y);
    bitmapCenterString(str, WIDTH_FIELD*.5,HEIGHT_FIELD*.1);
  }
}

void desenha_raquete(Racket raq) {
  glBegin(GL_POLYGON);
  glVertex2f(raq.x-WIDTH_RACKET*.5,raq.y-model.alturaRackets*.5);
  glVertex2f(raq.x+WIDTH_RACKET*.5,raq.y-model.alturaRackets*.5);
  glVertex2f(raq.x+WIDTH_RACKET*.5,raq.y+model.alturaRackets*.5);
  glVertex2f(raq.x-WIDTH_RACKET*.5,raq.y+model.alturaRackets*.5);
  glEnd();
}

void desenha_ball(Ball ball) {
  glPointSize(model.ball.tamanho);
  glBegin(GL_POINTS);
  glVertex2f(ball.x,ball.y);
  glEnd();
}

// ... definicao das rotinas auxiliares de desenho ...

// Callback de desenho

void Draw(void) {
  glClear(GL_COLOR_BUFFER_BIT);

  // ... chamada das rotinas auxiliares de desenho ...
  draw_field(model.players[0].score, model.players[1].score);

  glColor3f(1.0f, 0.0f, 0.0f);
  desenha_raquete(model.players[0]);

  glColor3f(0.0f, 0.0f, 1.0f);
  desenha_raquete(model.players[1]);

  glColor3f(1.0f, 1.0f, 0.0f);
  desenha_ball(model.ball);

  if(model.paused) {
    glColor3f(0.7,0.7,0.7);
    bitmapCenterString("Jogo Parado, use o menu para continuar",
        WIDTH_FIELD*.5,HEIGHT_FIELD*.5);
  }

  glFlush();
  if (state.doubleBuffer)
    glutSwapBuffers();
}

/*******************************
 ***   callbacks timer/idle  ***
 *******************************/

void Timer(int value) {
  glutTimerFunc(1000 / FPS, Timer, 0);
  // ... accoes do temporizador ... 

  if(state.activeMenu || model.paused)
    // sair em caso de o jogo estar paused ou menu estar activo
    return;

  // redesenhar o ecra 
  glutPostRedisplay();
}

void tick(double deltaTime) {
  float lowerLimit = HEIGHT_RACKET / 2.0;
  float upperLimit = HEIGHT_FIELD - lowerLimit;

  // HANDLE INPUTS
  //   JOGADOR1
  double accel = ACCEL_RACKET * deltaTime;
  if (state.keys.a == state.keys.q) {
    if (model.players[0].ySpeed > accel)
      model.players[0].ySpeed -= accel;
    else if (model.players[0].ySpeed < -accel)
      model.players[0].ySpeed += accel;
    else model.players[0].ySpeed = 0;
  } else if (state.keys.a && model.players[0].y >= lowerLimit) {
    model.players[0].ySpeed -= accel;
  } else if (state.keys.q && model.players[0].y <= upperLimit) {
    model.players[0].ySpeed += accel;
  }

  //   JOGADOR2
  if (state.keys.p == state.keys.l) {
    if (model.players[1].ySpeed > accel)
      model.players[1].ySpeed -= accel;
    else if (model.players[1].ySpeed < -accel)
      model.players[1].ySpeed += accel;
    else model.players[1].ySpeed = 0;
  } else if (state.keys.l && model.players[1].y >= lowerLimit) {
    model.players[1].ySpeed -= accel;
  } else if (state.keys.p && model.players[1].y <= upperLimit) {
    model.players[1].ySpeed += accel;
  }

  // CONSTRAINT PLAYERS SPEED
  model.players[0].ySpeed = MAX(MIN(model.players[0].ySpeed
        , VELOCITY_RACKET), -VELOCITY_RACKET);
  model.players[1].ySpeed = MAX(MIN(model.players[1].ySpeed
        , VELOCITY_RACKET), -VELOCITY_RACKET);

  // MOVE PLAYERS
  model.players[0].y += model.players[0].ySpeed * deltaTime;
  model.players[1].y += model.players[1].ySpeed * deltaTime;

  // CONSTRAINT PLAYERS TO THE ARENA
  model.players[0].y = MAX(MIN(model.players[0].y, upperLimit), lowerLimit);
  model.players[1].y = MAX(MIN(model.players[1].y, upperLimit), lowerLimit);

  if (model.players[0].y == upperLimit || model.players[0].y == lowerLimit)
    model.players[0].ySpeed = 0;
  if (model.players[1].y == upperLimit || model.players[1].y == lowerLimit)
    model.players[1].ySpeed = 0;

  // BALL MOVEMENT TICK
  float oldX = model.ball.x;
  float oldY = model.ball.y;
  model.ball.x += cos(model.ball.direction)
    * model.ball.speed * deltaTime;
  model.ball.y += sin(model.ball.direction)
    * model.ball.speed * deltaTime;

  // BALL COLLISION WITH TOP AND DOWN WALLS
  if (model.ball.y <= 0) {
    model.ball.y = 0;
    model.ball.direction = -model.ball.direction;
    model.ball.spin *= 0.4;
  } else if (model.ball.y >= HEIGHT_FIELD) {
    model.ball.y = HEIGHT_FIELD;
    model.ball.direction = -model.ball.direction;
    model.ball.spin *= 0.4;
  }

  float raqueteOffset = SERVICE_LINE + WIDTH_RACKET * 0.5;
  float limiteDireito = WIDTH_FIELD - raqueteOffset;

  int colisao = 0;
  float angle = 0.0f;

  // CHECK COLLISION WITH PLAYER 2
  if (oldX < limiteDireito && model.ball.x >= limiteDireito
      && model.ball.y >= model.players[1].y - HEIGHT_RACKET * 0.5
      && model.ball.y <= model.players[1].y + HEIGHT_RACKET * 0.5){
    model.ball.x = limiteDireito;
    colisao = 2;
    angle = (model.ball.y - model.players[1].y) / HEIGHT_RACKET;

    // CHECK COLLISION WITH PLAYER 1
  } else if (oldX > raqueteOffset && model.ball.x <= raqueteOffset
      && model.ball.y >= model.players[0].y - HEIGHT_RACKET * 0.5
      && model.ball.y <= model.players[0].y + HEIGHT_RACKET * 0.5) {
    model.ball.x = raqueteOffset;
    colisao = 1;
    angle = (model.ball.y - model.players[0].y) / (HEIGHT_RACKET * 0.5);

    // CHECK COLLISION WITH TOP AND BOTTOM OF PLAYER 2
  } else if (model.ball.x >= limiteDireito
      && model.ball.x <= limiteDireito + WIDTH_RACKET) {
    if(oldY > model.players[1].y + HEIGHT_RACKET * 0.5
        && model.ball.y <= model.players[1].y + HEIGHT_RACKET * 0.5) {
      model.ball.direction = - model.ball.direction;
      model.ball.y = model.players[1].y + HEIGHT_RACKET * 0.5;
    } else if (oldY < model.players[1].y - HEIGHT_RACKET * 0.5
        && model.ball.y >= model.players[1].y - HEIGHT_RACKET * 0.5) {
      model.ball.direction = - model.ball.direction;
      model.ball.y = model.players[1].y - HEIGHT_RACKET * 0.5;
    }

    // CHECK COLLISION WITH TOP AND BOTTOM OF PLAYER 1
  } else if (model.ball.x <= raqueteOffset
      && model.ball.x > raqueteOffset - WIDTH_RACKET) {
    if (oldY > model.players[0].y + HEIGHT_RACKET * 0.5
        && model.ball.y <= model.players[0].y + HEIGHT_RACKET * 0.5) {
      model.ball.direction = - model.ball.direction;
      model.ball.y = model.players[0].y + HEIGHT_RACKET * 0.5;
    } else if (oldY < model.players[0].y - HEIGHT_RACKET * 0.5
        && model.ball.y >= model.players[0].y - HEIGHT_RACKET * 0.5) {
      model.ball.direction = - model.ball.direction;
      model.ball.y = model.players[0].y - HEIGHT_RACKET * 0.5;
    }
  }

  // ACT IF THERE WAS A COLLISION RELEVANT TO THE GAME
  if (colisao) {
    // REVERSE ANGLE
    model.ball.direction = reverse_angle(model.ball.direction);
    model.ball.speed *= 1.1;
    model.ball.speed = MIN(model.ball.speed, MAX_BALL_VELOCITY);

    // ADJUST ANGLE TO THE WAY IT COLLIDED WITH THE PLAYER
    bool reverse = false;
    if(model.ball.direction < -M_PI / 2.0
        || model.ball.direction > M_PI / 2.0) {
      reverse = true;
      model.ball.direction = reverse_angle(model.ball.direction);
    }
    model.ball.direction += RAD(BALL_DIRECTION_CHANGE) * angle;

    if (model.ball.direction > RAD(ANGLE_LIMIT))
      model.ball.direction = RAD(ANGLE_LIMIT);
    if (model.ball.direction < -RAD(ANGLE_LIMIT))
      model.ball.direction = -RAD(ANGLE_LIMIT);
    if (reverse)
      model.ball.direction = reverse_angle(model.ball.direction);

    // SET BALL'S CURVING SPEED
    float influence = (1.0 - abs(angle)) * INFLUENCE_SPIN;
    float speed = colisao == 1
      ? model.players[0].ySpeed
      : model.players[1].ySpeed;
    float spin = speed * influence / BALL_VELOCITY;
    model.ball.spin = colisao == 1 ? -spin : spin;
  }

  // UPDATE BALLS DIRECTION AND SPIN
  bool right = model.ball.direction <= RAD(80.0)
    && model.ball.direction >= -RAD(80.0);
  model.ball.direction += model.ball.spin * deltaTime;
  model.ball.spin *= MAX(1.0 - FADEAWAY_SPIN * deltaTime, 0);
  if (!right)
    model.ball.direction = reverse_angle(model.ball.direction);
  model.ball.direction =
    MAX(MIN(model.ball.direction, RAD(ANGLE_LIMIT)), -RAD(ANGLE_LIMIT));
  if (!right)
    model.ball.direction = reverse_angle(model.ball.direction);

  bool needsReset = false;
  if (model.ball.x < 0) {
    model.players[1].score++;
    needsReset = true;
  } else if (model.ball.x > WIDTH_FIELD) {
    model.players[0].score++;
    needsReset = true;
  }
  if (needsReset) {
    start_game();
    model.ball.speed = BALL_VELOCITY;
  }
}

void TimerTick(int value) {
  static const double deltaTime = 1.0 / TICKS_PER_SECOND;
  glutTimerFunc(deltaTime, TimerTick, 0);

  // Calculate delta time
  static int lastTime = glutGet(GLUT_ELAPSED_TIME);
  static double cummulativeTime = 0.0;

  int currentTime = glutGet(GLUT_ELAPSED_TIME);
  cummulativeTime += (currentTime - lastTime) / 1000.0;
  lastTime = currentTime;

  if(state.activeMenu || model.paused)
    return;

  while (cummulativeTime >= deltaTime) {
    tick(deltaTime);
    cummulativeTime -= deltaTime;
  }
}

void imprime_ajuda(void) {
  printf("\n\nDesenho de um quadrado\n");
  printf("h,H - Ajuda \n");
  printf("q,Q - Jogador 1 cima\n");
  printf("a,A - Jogador 1 baixo\n");
  printf("P,p - Jogador 2 cima\n");
  printf("L,l - Jogador 2 baixo\n");
  printf("D,d - Debug On/OFF\n");
  printf("ESC - Sair\n");
}

/*******************************
 ***  callbacks de teclado    ***
 *******************************/

// Callback para interaccao via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)  {
  switch (key) {
    case 27:
      exit(1);
      // ... accoes sobre outras keys ... 

    case 'h' :
    case 'H' :
      imprime_ajuda();
      break;
    case 'i' :
    case 'I' :
      start_game();
      break;
    case 'Q' : 
    case 'q' : state.keys.q=GL_TRUE;
               break; 
    case 'A' : 
    case 'a' : state.keys.a=GL_TRUE;
               break;
    case 'P' : 
    case 'p' : state.keys.p=GL_TRUE;
               break;
    case 'L' : 
    case 'l' : state.keys.l=GL_TRUE;
               break;
    case 'D' : 
    case 'd' : state.debug=!state.debug;
               if(state.activeMenu || model.paused)
                 glutPostRedisplay();
               printf("DEBUG is %s\n",(state.debug)?"ON":"OFF");
               break;

  }

  if(state.debug)
    printf("Carregou na tecla %c\n",key);
}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y) {
  switch (key) {
    // ... accoes sobre largar keys ... 
    case 'Q' : 
    case 'q' : state.keys.q=GL_FALSE;
               break; 
    case 'A' : 
    case 'a' : state.keys.a=GL_FALSE;
               break;
    case 'P' : 
    case 'p' : state.keys.p=GL_FALSE;
               break;
    case 'L' : 
    case 'l' : state.keys.l=GL_FALSE;
               break;
  }

  if(state.debug)
    printf("Largou a tecla %c\n",key);
}

// Callback para interaccao via keys especiais  (carregar na tecla)

void MenuStatus(int status, int x, int y) {
  /* status => GLUT_MENU_IN_USE, GLUT_MENU_NOT_IN_USE 
     x,y    => coordenadas do ponteiro quando se entra no menu
     */

  if(status==GLUT_MENU_IN_USE) 
    state.activeMenu=GL_TRUE;
  else
    state.activeMenu=GL_FALSE;

  if(state.debug)
    printf("MenuStatus status:%d coord:%d %d\n",status,x,y);
}

void menu(int opcao) {
  switch (opcao) {
    case 0 :
      exit(1);
      break;
      // speed ball
    case 1 :
      if(model.ball.speed>2)
        model.ball.speed-=2;
      break;
    case 2 :
      if(model.ball.speed<16)
        model.ball.speed+=2;
      break;
      // tamanho ball
    case 3 :
      if(model.ball.tamanho>5)
        model.ball.tamanho--;
      break;
    case 4 :
      if(model.ball.tamanho<15)
        model.ball.tamanho++;
      break;
      // speed raquete
    case 5 :
      if(model.speedRackets>10)
        model.speedRackets-=5;
      break;
    case 6 :
      if(model.speedRackets<30)
        model.speedRackets+=5;
      break;
      // tamanho raquete
    case 7 :
      if(model.alturaRackets>30)
        model.alturaRackets-=10;
      break;
    case 8 :
      if(model.speedRackets<100)
        model.alturaRackets+=10;
      break;
    case 9 :
      model.paused=!model.paused;
      if(model.paused) {
        glutChangeToMenuEntry(1,"Começar",9);
        glutPostRedisplay();
      } else {
        glutChangeToMenuEntry(1,"Parar",9);
      }
      break;
  }
}

void cria_menu() {
  // funçoes de manipulação de menus
  // id=glutCreateMenu(menu) - cria um menu tratado pela funcao menu e
  //                              devolve o id
  // glutDestroyMenu(id)     - destroi o menu identificado por id
  // id=glutGetMenu()        - devolve o id do menu actual
  // glutSetMenu(id)         - torna actual o menu com o id
  //
  // glutAddMenuEntry("texto", valor) -  adiciona uma entrada ao menu actual
  // glutChangeToMenuEntry(item, "novo texto", novo_valor) 
  //                             -  altera a entrada item(1,2,3..)
  //                                    do menu actual
  // glutAddSubMenu("texto", id) -  adiciona o submenu id ao menu actual
  // glutChangeToSubMenu(item, "novo texto", novo_id) 
  //                          -  altera a entrada submenu item(1,2,3..)
  //                                  do menu actual
  // glutRemoveMenuItem(item) - apaga a entrada item(1,2,3...)
  //
  // glutAttachMenu(botao) - associa o menu actual ao botao 
  // glutDetachMenu(botao) - desassocia o menu associado
  //                          ao botao GLUT_RIGHT_BUTTON
  // botao = GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON ou GLUT_RIGHT_BUTTON

  /* DEFINIR MENUS */
  // regista função que trata do menu
  state.menu_vel_ball_id = glutCreateMenu(menu);
  glutAddMenuEntry("Diminuir", 1);
  glutAddMenuEntry("Aumentar", 2);

  // regista função que trata do menu
  state.menu_tam_ball_id = glutCreateMenu(menu);
  glutAddMenuEntry("Diminuir", 3);
  glutAddMenuEntry("Aumentar", 4);

  // regista função que trata do menu
  state.menu_vel_raq_id = glutCreateMenu(menu);
  glutAddMenuEntry("Diminuir", 5);
  glutAddMenuEntry("Aumentar", 6);

  // regista função que trata do menu
  state.menu_tam_raq_id = glutCreateMenu(menu);
  glutAddMenuEntry("Diminuir", 7);
  glutAddMenuEntry("Aumentar", 8);

  // regista função que trata do menu
  state.menu_id=glutCreateMenu(menu);
  glutAddMenuEntry("Parar", 9);

  // adicionar submenus
  glutAddSubMenu("Velocidade da ball", state.menu_vel_ball_id);
  glutAddSubMenu("Tamanho da ball", state.menu_tam_ball_id);
  glutAddSubMenu("Velocidade da raquete", state.menu_vel_raq_id);
  glutAddSubMenu("Altura da raquete",state.menu_tam_raq_id);

  glutAddMenuEntry("Sair", 0);

  // Agarrar menu actual ao botão
  glutAttachMenu(GLUT_RIGHT_BUTTON);
}


int main(int argc, char **argv) {
  state.doubleBuffer=GL_TRUE;

  glutInit(&argc, argv);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(WIDTH_FIELD*3, HEIGHT_FIELD*3);
  glutInitDisplayMode(
      (state.doubleBuffer ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);
  if (glutCreateWindow("Ping-Pong") == GL_FALSE)
    exit(1);

  init();

  imprime_ajuda();

  // Registar callbacks do GLUT

  // callbacks de janelas/desenho
  glutReshapeFunc(Reshape);
  glutDisplayFunc(Draw);

  // Callbacks de teclado
  glutKeyboardFunc(Key);
  glutKeyboardUpFunc(KeyUp);
  //glutSpecialFunc(SpecialKey);
  //glutSpecialUpFunc(SpecialKeyUp);

  // callbacks timer/idle
  glutTimerFunc(0, Timer, 0);
  glutTimerFunc(0, TimerTick, 0);

  //Menus
  cria_menu();
  glutMenuStatusFunc(MenuStatus);

  // COMECAR...
  glutMainLoop();

  return 0;
}

