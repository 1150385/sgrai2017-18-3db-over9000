#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <errno.h>
#include <limits.h>

#define SYS_ERR(msg) { perror(msg); exit(EXIT_FAILURE); }

#define BUFFER_SIZE 512

unsigned long read_integer(FILE* input);
double read_double(FILE* input);
double get_distance(double *point1, double *point2);

int main() {
  // Exercise variables
  unsigned long int integer;
  double p[2];
  double points[2][2];
  double center[2];

  // Ex 1
  // ===========================================================
  printf("Exercise 1.\n");
  printf("Please enter an integer: ");
  fflush(stdout);
  integer = read_integer(stdin);
  printf("You entered the integer: %lu\n\n", integer);

  // Ex 2
  // ===========================================================
  printf("Exercise 2.\n");
  printf("Please enter 2 floating point numbers.\n");
  for (int i=0; i<2; i++) {
    printf("Point %d\n", i);
    for (int j=0; j<2; j++) {
      printf("  %s: ", j==0?"x":"y");
      fflush(stdout);
      points[i][j] = read_double(stdin);
    }
  }
  printf("You entered the points {{");
  for (int i=0; i<2; i++) {
    for (int j=0; j<2; j++) {
      printf("%f%s", points[i][j], j==0?"; ":"");
    }
    if (i==0) printf("}; {");
  }
  printf("}}\n\n");

  // Ex 3
  // ===========================================================
  printf("Exercise 3.\n");
  center[0] = (points[0][0] + points[1][0]) / 2;
  center[1] = (points[0][1] + points[1][1]) / 2;
  printf("Medium point is {%f, %f}\n\n", center[0], center[1]);

  // Ex 4
  // ===========================================================
  printf("Exercise 4.\n");
  double distance = get_distance(points[0], points[1]);
  printf("Distance between those two points: %f\n\n", distance);

  // Ex 5
  // ===========================================================
  double radius = distance / 2.0;
  double angle = (2*M_PI) / integer;
  printf("The circunference will have the following points:\n");
  for (int i=0; i<integer; i++) {
    double x = cos(angle*i)*radius + center[0];
    double y = sin(angle*i)*radius + center[1];
    printf("Point %d: {%f; %f}\n", i+1, x, y);
  }

  return 0;
}

double get_distance(double *point1, double *point2) {
  return sqrt(pow(point2[0] - point1[0], 2.0) + pow(point2[1] - point1[1], 2.0));
}

unsigned long read_integer(FILE* input) {
  // Auxiliary variables
  char buffer[BUFFER_SIZE];
  char *end_buffer;

  fgets(buffer, BUFFER_SIZE, stdin);
  end_buffer = &buffer[strlen(buffer)-1];
  unsigned long res = strtoul(buffer, &end_buffer, 10);
  if (res == ULONG_MAX && errno == ERANGE) SYS_ERR("strtoul");
  return res;
}

double read_double(FILE* input) {
  // Auxiliary variables
  char buffer[BUFFER_SIZE];
  char *end_buffer;

  fgets(buffer, BUFFER_SIZE, stdin);
  end_buffer = &buffer[strlen(buffer)-1];
  double res = strtod(buffer, &end_buffer);
  if (res == HUGE_VAL && errno == ERANGE) SYS_ERR("strotod");
  return res;
}

