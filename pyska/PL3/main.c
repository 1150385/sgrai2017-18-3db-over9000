#include <GL/glut.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

#define STEPS 600
#define STEP_MOD 50
#define STEP_MOD_SMALL 10

#define FPS 10

struct estado_tempo {
  unsigned hours, minutes, seconds, millis;
};

struct estado_tempo estado;
int num_vertices = 60;

void poligono(int n, float x0, float y0, float r) {
  float angle = 2.0 * M_PI / n;
  glBegin(GL_TRIANGLE_FAN);
  for (int i=0; i<n; i++) {
    float x = cos(angle * i) * r + x0;
    float y = sin(angle * i) * r + y0;
    glVertex2f(x, y);
  }
  glEnd();
}

void update_estado() {
  struct timespec curr_time;
  if (clock_gettime(CLOCK_REALTIME, &curr_time) != 0) {
    memset(&curr_time, 0, sizeof(struct timespec));
  };
  estado.millis = curr_time.tv_nsec / 1E6;
  estado.seconds = curr_time.tv_sec;
  estado.minutes = estado.seconds / 60;
  estado.hours = (estado.minutes / 60) % 24 + 1;
  estado.minutes %= 60;
  estado.seconds %= 60;
}

void onTimer(int value) {
  (void) value;
  glutPostRedisplay();
  update_estado();
  glutTimerFunc(1000 / FPS, onTimer, 0);
}

void keyboard(unsigned char key, int x, int y) {
  (void) x;
  (void) y;
  switch (key) {
    case 27:
      exit(0);
    case 'w':
      if (num_vertices < 60)
        num_vertices++;
      break;
    case 's':
      if (num_vertices > 3)
        num_vertices--;
      break;
  }
  glutPostRedisplay();
}

void mostrador(float x0, float y0, float r) {
  double angle = 2.0 * M_PI / STEPS;
  for (int i=0; i<STEPS; i++) {
    glBegin(GL_LINES);
    double percent = (i % STEP_MOD == 0 ? 0.75 : (i % STEP_MOD_SMALL == 0 ? 0.85 : 0.9));
    double x1 = cos(angle * i) * r * percent + x0;
    double y1 = sin(angle * i) * r * percent + y0;
    double x2 = cos(angle * i) * r + x0;
    double y2 = sin(angle * i) * r + y0;
    glVertex2f(x1, y1);
    glVertex2f(x2, y2);
    glEnd();
  }
}

void ponteiro(float x0, float y0, float angle, float length, float width) {
  glLineWidth(width);
  glBegin(GL_LINES);
  double seconds_x = sin(angle) * length + x0;
  double seconds_y = cos(angle) * length + y0;
  glVertex2f(x0, y0);
  glVertex2f(seconds_x, seconds_y);
  glEnd();
}

void draw() {
  glClearColor(0.2, 0.2, 0.2, 0.0);
//  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);
  glLineWidth(1.0);

  glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

  double seconds_angle = 2.0 * M_PI * estado.seconds / 60.0;
  double minutes_angle = (2.0 * M_PI * estado.minutes / 60.0) + seconds_angle / (20.0 * M_PI);
  double hours_angle = (2.0 * M_PI * estado.hours / 12.0) + minutes_angle / (4.0 * M_PI);

  double center[2] = {0.0, 0.0};
  double radius = 0.8;

  glColor3f(0.1, 0.0, 0.45);
  poligono(num_vertices, center[0], center[1], radius * 1.01);

  center[1] = -0.3;
  radius = 0.20;
  glLineWidth(1.0);

  glColor3f(0.0, 0.0, 0.0);
  poligono(num_vertices, center[0], center[1], radius * 1.01);

  glColor3f(0.1, 0.0, 0.45);
  mostrador(center[0], center[1], radius);

  ponteiro(center[0], center[1], seconds_angle, radius * 0.8, 1.0); // SECONDS
  ponteiro(center[0], center[1], minutes_angle, radius * 0.5, 2.0); // MINUTES
  ponteiro(center[0], center[1], hours_angle, radius * 0.3, 3.0); // HOURS

  center[1] = 0.0;
  radius = 0.8;

  glLineWidth(1.0);
  glColor3f(1.0, 1.0, 1.0);
  mostrador(center[0], center[1], radius);

  ponteiro(center[0], center[1], seconds_angle, radius * 0.8, 1.0); // SECONDS
  ponteiro(center[0], center[1], minutes_angle, radius * 0.5, 2.0); // MINUTES
  ponteiro(center[0], center[1], hours_angle, radius * 0.3, 3.0); // HOURS

  glFlush();
}

void init(void) {
  update_estado();
  glutTimerFunc(0, onTimer, 0);
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(500, 500);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("OpenGL - Creating a clock");
  glutDisplayFunc(draw);
  glutKeyboardFunc(keyboard);
  init();
  glutMainLoop();
  return 0;
}
