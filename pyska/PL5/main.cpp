#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG 1
#define LADO_MAXIMO     2
#define LADO_MINIMO     0.3
#define DELTA_LADO      0.1

#define ROTATION_SPEED  45
#define TRANSLATION_SPEED 0.5

#define FPS               60
#define TICKS_PER_SECOND  120

/* VARIAVEIS GLOBAIS */

typedef struct {
  GLboolean   doubleBuffer;
  GLboolean   debug;
  GLboolean    movimentoTranslacao;      // se os cubinhos se movem
  GLboolean   movimentoRotacao;         // se o cubo grande roda;
}Estado;


typedef struct {
  GLfloat   theta[3];     // 0-Rotação em X; 1-Rotação em Y; 2-Rotação em Z

  GLint     eixoRodar;    // eixo que está a rodar (mudar com o rato)
  GLfloat   ladoCubo;     // comprimento do lado do cubo
  GLfloat   escala;
  GLfloat   deltaRotacao; // incremento a fazer ao angulo quando roda
  GLboolean  sentidoTranslacao; //sentido da translação dos cubos pequenos
  GLfloat    translacaoCubo; //
  GLfloat   deltaTranslacao; // incremento a fazer na translacao
  GLboolean sentidoRotacao;  //sentido da rotação dos cubos pequenos
  GLfloat   escalaCubos;    // escala dos pequenos cubos
  GLfloat   thetaCubo;     // Rotação dos cubinhos
}Modelo;

Estado estado;
Modelo modelo;


/* Inicialização do ambiente OPENGL */
void inicia_modelo() {
  estado.movimentoTranslacao=GL_FALSE;
  estado.movimentoRotacao=GL_FALSE;

  modelo.theta[0]=0;
  modelo.theta[1]=0;
  modelo.theta[2]=0;
  modelo.eixoRodar=0;  // eixo de X;
  modelo.ladoCubo=1;
  modelo.escala=1.0;
  modelo.escalaCubos=0.25;
  modelo.translacaoCubo=(modelo.escalaCubos+1)*modelo.ladoCubo;
}

void Init(void) {
  inicia_modelo();
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
}

/**************************************
 ***  callbacks de janela/desenho    ***
 **************************************/

/* CALLBACK PARA REDIMENSIONAR JANELA */
void Reshape(int width, int height) {
  glViewport(0, 0, (GLint) width, (GLint) height);  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (width < height)
    glOrtho(-5, 5, -5*(GLdouble)height/width, 5*(GLdouble)height/width,-10,10);
  else
    glOrtho(-5*(GLdouble)width/height, 5*(GLdouble)width/height,-5, 5, -10,10);


  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


/* ... definicao das rotinas auxiliares de desenho ... */

void desenhaPoligono(GLfloat a[], GLfloat b[],
                     GLfloat c[], GLfloat  d[], GLfloat cor[]) {
  /* draw a polygon via list of vertices */
  glBegin(GL_POLYGON);
  glColor3fv(cor);
  glVertex3fv(a);
  glVertex3fv(b);
  glVertex3fv(c);
  glVertex3fv(d);
  glEnd();
}

void desenhaLinha(GLfloat a[], GLfloat b[], GLfloat cor[]) {
  glBegin(GL_LINES);
  glColor3fv(cor);
  glVertex3fv(a);
  glVertex3fv(b);
  glEnd();
}

void cubo() {
  GLfloat vertices[][3] = {
    {-0.5,-0.5,-0.5}, 
    { 0.5,-0.5,-0.5}, 
    { 0.5, 0.5,-0.5}, 
    {-0.5, 0.5,-0.5},
    {-0.5,-0.5, 0.5}, 
    { 0.5,-0.5, 0.5}, 
    { 0.5, 0.5, 0.5}, 
    {-0.5, 0.5, 0.5}};

  GLfloat cores[][3] = {
    {0.0,1.0,1.0},
    {1.0,0.0,0.0},
    {1.0,1.0,0.0}, 
    {0.0,1.0,0.0}, 
    {1.0,0.0,1.0}, 
    {0.0,0.0,1.0}, 
    {1.0,1.0,1.0}};

  GLint order[][4] = {
    {0, 3, 2, 1},
    {0, 1, 5, 4},
    {0, 4, 7, 3},
    {6, 5, 1, 2},
    {6, 7, 4, 5},
    {6, 2, 3, 7}};

  for (int i=0; i<6; i++) {
    desenhaPoligono(vertices[order[i][0]], vertices[order[i][1]],
                    vertices[order[i][2]], vertices[order[i][3]], cores[i]);
  }
}

void cubo1() {
  GLfloat vertices[][3] = {
    {-0.5,-0.5, 0.0}, 
    { 0.5,-0.5, 0.0}, 
    { 0.5, 0.5, 0.0}, 
    {-0.5, 0.5, 0.0}};

  GLfloat cores[][3] = {
    {0.0,1.0,1.0},
    {1.0,0.0,0.0},
    {1.0,1.0,0.0}, 
    {0.0,1.0,0.0}, 
    {1.0,0.0,1.0}, 
    {0.0,0.0,1.0},
    {1.0,1.0,1.0}};

  glPushMatrix();
  for (int i=0; i<4; i++) {
    glRotatef(90, 1, 0, 0);
    glPushMatrix();
    glTranslatef(0.0, 0.0, 0.5);
    desenhaPoligono(vertices[0], vertices[1], vertices[2], vertices[3], cores[i]);
    glPopMatrix();
  }
  glPopMatrix();
  glPushMatrix();
  for (int i=0; i<2; i++) {
    glRotatef(i==0?90:180, 0, 1, 0);
    glPushMatrix();
    glTranslatef(0.0, 0.0, 0.5);
    desenhaPoligono(vertices[0], vertices[1], vertices[2], vertices[3], cores[i+4]);
    glPopMatrix();
  }
  glPopMatrix();
}

void eixos() {
  GLfloat vertices[][3] = {
    {0.0, 0.0, 0.0},
    {1.0, 0.0, 0.0},
    {0.0, 1.0, 0.0},
    {0.0, 0.0, 1.0}};

  GLfloat cores[][3] = {
    {1.0, 0.0, 0.0},
    {0.0, 1.0, 0.0},
    {0.0, 0.0, 1.0}};

  GLint order[][2] = {
    {0, 1},
    {0, 2},
    {0, 3}};

  for (int i=0; i<3; i++) {
    desenhaLinha(vertices[order[i][0]], vertices[order[i][1]], cores[i]);
  }
}


/* Callback de desenho */
void Draw(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glPolygonMode(GL_BACK, GL_LINE);
  glPushMatrix();
  glRotatef(modelo.theta[0],1,0,0);
  glRotatef(modelo.theta[1],0,1,0);
  glRotatef(modelo.theta[2],0,0,1);
  glScalef(modelo.escala, modelo.escala, modelo.escala);

  cubo1();
  glPushMatrix();
  glScalef(2.0, 2.0, 2.0);
  eixos();
  glPopMatrix();

  for (int i=0; i<3; i++) {
    glPushMatrix();
    glTranslatef((i==0)*modelo.translacaoCubo,
                 (i==1)*modelo.translacaoCubo,
                 (i==2)*modelo.translacaoCubo);
    glScalef(modelo.escalaCubos, modelo.escalaCubos, modelo.escalaCubos);
    glRotatef(modelo.thetaCubo, i==0, i==1, i==2);
    cubo1();
    glPopMatrix();
  }
  glPopMatrix();

  glPushMatrix();
  glTranslatef(3.5, -3.5, 0.0);
  glRotatef(modelo.theta[0],1,0,0);
  glRotatef(modelo.theta[1],0,1,0);
  glRotatef(modelo.theta[2],0,0,1);
  eixos();
  glPopMatrix();


  if (estado.doubleBuffer)
    glutSwapBuffers();
  else
    glFlush();

}

void Timer(int value) {
  glutTimerFunc(1000 / FPS, Timer, 0);
  // ... accoes do temporizador ... 

  // redesenhar o ecra 
  glutPostRedisplay();
}

void tick(double deltaTime) {
  if (estado.movimentoRotacao)
    modelo.theta[modelo.eixoRodar] += deltaTime * ROTATION_SPEED;

  if (modelo.translacaoCubo < (modelo.ladoCubo * (modelo.escalaCubos+1))/2) {
    modelo.sentidoTranslacao = true;
  } else if (modelo.translacaoCubo > modelo.ladoCubo+1) {
    modelo.sentidoTranslacao = false;
  }

  modelo.translacaoCubo += deltaTime
    * TRANSLATION_SPEED
    * (modelo.sentidoTranslacao ? 1.0 : -1.0);
  
  if (modelo.sentidoTranslacao == false) {
    modelo.thetaCubo += deltaTime * ROTATION_SPEED * 10;
  }
}

/*******************************
 ***   callbacks timer   ***
 *******************************/
/* Callback de temporizador */
void TimerTick(int value) {
  static const double deltaTime = 1.0 / TICKS_PER_SECOND;
  glutTimerFunc(deltaTime, TimerTick, 0);

  // Calculate delta time
  static int lastTime = glutGet(GLUT_ELAPSED_TIME);
  static double cummulativeTime = 0.0;

  int currentTime = glutGet(GLUT_ELAPSED_TIME);
  cummulativeTime += (currentTime - lastTime) / 1000.0;
  lastTime = currentTime;

  while (cummulativeTime >= deltaTime) {
    tick(deltaTime);
    cummulativeTime -= deltaTime;
  }
}

/*******************************
 ***  callbacks de teclado    ***
 *******************************/

void imprime_ajuda(void)
{
  printf("\n\nDesenho de um quadrado\n");
  printf("h,H - Ajuda \n");
  printf("F1  - Reiniciar \n");
  printf("F2  - Poligono Fill \n");
  printf("F3  - Poligono Line \n");
  printf("F4  - Poligono Point \n");
  printf("+   - Aumentar tamanho dos Cubos\n");
  printf("-   - Diminuir tamanho dos Cubos\n");
  printf("i,I - Reiniciar Variáveis\n");
  printf("p,p - Iniciar/Parar movimento dos cubinhos\n");
  printf("ESC - Sair\n");
  printf("teclas do rato para iniciar/parar rotação e alternar eixos\n");

}


/* Callback para interaccao via teclado (carregar na tecla) */
void Key(unsigned char key, int x, int y)
{
  switch (key) {
    case 27:
      exit(1);
      /* ... accoes sobre outras teclas ... */

    case 'h' :
    case 'H' :
      imprime_ajuda();
      break;
    case '+':
      if(modelo.escala<LADO_MAXIMO)
      {
        modelo.escala+=DELTA_LADO;
        glutPostRedisplay();
      }
      break;

    case '-':
      if(modelo.escala>LADO_MINIMO)
      {
        modelo.escala-=DELTA_LADO;
        glutPostRedisplay();
      }
      break;

    case 'i' :
    case 'I' :
      inicia_modelo();
      glutPostRedisplay();
      break;
    case 'p' :
    case 'P' :
      estado.movimentoTranslacao=!estado.movimentoTranslacao;
      break;

  }

  if(DEBUG)
    printf("Carregou na tecla %c\n",key);

}

/* Callback para interaccao via teclado (largar a tecla) */
void KeyUp(unsigned char key, int x, int y)
{

  if(DEBUG)
    printf("Largou a tecla %c\n",key);
}

/* Callback para interaccao via teclas especiais  (carregar na tecla) */
void SpecialKey(int key, int x, int y)
{
  /* ... accoes sobre outras teclas especiais ... 
     GLUT_KEY_F1 ... GLUT_KEY_F12
     GLUT_KEY_UP
     GLUT_KEY_DOWN
     GLUT_KEY_LEFT
     GLUT_KEY_RIGHT
     GLUT_KEY_PAGE_UP
     GLUT_KEY_PAGE_DOWN
     GLUT_KEY_HOME
     GLUT_KEY_END
     GLUT_KEY_INSERT 
     */

  switch (key) {

    /* redesenhar o ecra */
    //glutPostRedisplay();
    case GLUT_KEY_F1 :
      inicia_modelo();
      glutPostRedisplay();
      break;

  }


  if(DEBUG)
    printf("Carregou na tecla especial %d\n",key);
}

/* Callback para interaccao via teclas especiais (largar na tecla) */
void SpecialKeyUp(int key, int x, int y)
{

  if(DEBUG)
    printf("Largou a tecla especial %d\n",key);

}

/*******************************
 ***  callbacks do rato       ***
 *******************************/

void MouseMotion(int x, int y)
{
  /* x,y    => coordenadas do ponteiro quando se move no rato
     a carregar em teclas
     */

  if(DEBUG)
    printf("Mouse Motion %d %d\n",x,y);

}

void MousePassiveMotion(int x, int y)
{
  /* x,y    => coordenadas do ponteiro quando se move no rato
     sem estar a carregar em teclas
     */

  if(DEBUG)
    printf("Mouse Passive Motion %d %d\n",x,y);

}

void Mouse(int button, int state, int x, int y)
{
  /* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
     state  => GLUT_UP, GLUT_DOWN
     x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
     */

  // alterar o eixo que roda (variável modelo.eixoRodar)

  switch(button){
    case GLUT_LEFT_BUTTON :      
      if(state == GLUT_DOWN)
      {        
        modelo.eixoRodar = (modelo.eixoRodar + 1) % 3;
      }
      break;
    case GLUT_MIDDLE_BUTTON :      
      if(state == GLUT_DOWN)
      {        
      }
      break;
    case GLUT_RIGHT_BUTTON :
      if(state == GLUT_DOWN)
      {        
        estado.movimentoRotacao = !estado.movimentoRotacao;
      }
      break;
  }
  if(DEBUG)
    printf("Mouse button:%d state:%d coord:%d %d\n",button,state,x,y);
}
int main(int argc, char **argv)
{
  char str[]=" makefile MAKEFILE Makefile ";

  estado.doubleBuffer=1;
  glutInit(&argc, argv);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(400, 400);
  glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE)
      | GLUT_RGB | GLUT_DEPTH);
  if (glutCreateWindow("Exemplo") == GL_FALSE)
    exit(1);

  Init();
  imprime_ajuda();
  /* Registar callbacks do GLUT */

  /* callbacks de janelas/desenho */
  glutReshapeFunc(Reshape);
  glutDisplayFunc(Draw);

  /* Callbacks de teclado */
  glutKeyboardFunc(Key);
  //glutKeyboardUpFunc(KeyUp);
  glutSpecialFunc(SpecialKey);
  //glutSpecialUpFunc(SpecialKeyUp);

  /* callbacks rato */
  //glutPassiveMotionFunc(MousePassiveMotion);
  //glutMotionFunc(MouseMotion);
  glutMouseFunc(Mouse);

  /* callbacks timer/idle */
  glutTimerFunc(0, Timer, 0);
  glutTimerFunc(0, TimerTick, 0);
  //glutIdleFunc(Idle);


  /* COMECAR... */
  glutMainLoop();
  return 0;
}

